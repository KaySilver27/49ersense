package example.org.smartgridscheduler;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AddAppliance extends Activity{
    private Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_appliance);

        bundle = getIntent().getExtras();
        final String app_name = bundle.getString("app_name");
        final String username = bundle.getString("username");
        final Button backButton = (Button) findViewById(R.id.button_back);

        final EditText appName = (EditText) findViewById(R.id.app_name);

        if(app_name.equals("Other")){
            appName.setText("");
            appName.setHint("Enter Appliance Name:");
        }
        else appName.setText(app_name);

        final EditText startTime = (EditText) findViewById(R.id.startTime);
        final EditText endTime = (EditText) findViewById(R.id.endTime);
        final EditText duration = (EditText) findViewById(R.id.editText9);
        final EditText watt = (EditText) findViewById(R.id.watt);

        final Button submitButton = (Button) findViewById(R.id.btn_submit);

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                String strt_time = startTime.getText().toString();
                String end_time = endTime.getText().toString();
                String dur = duration.getText().toString();
                String watt_consumed = watt.getText().toString();
                String name = appName.getText().toString();

                String status = getConnection(name, strt_time, end_time, dur, watt_consumed, username);
                if(status.equals("true\n")){
                    Toast.makeText(getBaseContext(), "Appliance Added!", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                    Toast.makeText(getBaseContext(),"Problem! Server unavailable",Toast.LENGTH_SHORT).show();
            }
        });
    }
    public String getConnection(String appliance, String start, String end, String runTime, String power, String houseID){

        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("appName",appliance));
        nameValuePairs1.add(new BasicNameValuePair("startTime",start));
        nameValuePairs1.add(new BasicNameValuePair("endTime",end));
        nameValuePairs1.add(new BasicNameValuePair("duration",runTime));
        nameValuePairs1.add(new BasicNameValuePair("watt",power));
        nameValuePairs1.add(new BasicNameValuePair("houseID",houseID));

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();

            // have to change the ip here to correct ip
            HttpPost httppost = new HttpPost(MainActivity.ipAddress+"addAppliance.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection " + e.toString());
            Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();
            return "";
        }
        //convert response to string
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result "+e.toString());
        }
        return result;
    }
}