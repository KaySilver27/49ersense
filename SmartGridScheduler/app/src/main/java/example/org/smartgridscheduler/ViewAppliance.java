package example.org.smartgridscheduler;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ViewAppliance extends Activity{

    private Handler handler;
    private Bundle bundle;
    Appliance appliance;

   // static ArrayList<Appliance> appliances = new ArrayList<Appliance>();
    static ArrayList<String> appList = new ArrayList<String>();

    private String[] attributes = {"Appliance Name","Start Time", "End Time", "Duration","Watts"};

    static int startTime, endTime, duration, watts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_appliance);

        ArrayList<Appliance> appliances = new ArrayList<Appliance>();
        bundle = getIntent().getExtras();
        String user = bundle.getString("username");

        final TextView show = (TextView) findViewById(R.id.xmlTextView);
        final Button bckbutton = (Button) findViewById(R.id.button);

        bckbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String conResult = getConnection (MainActivity.ipAddress+"viewAppliance.php","retrieve",user);

        try{
            JSONArray jsArray = new JSONArray(conResult);
            for(int i=0;i<jsArray.length();i++){

                JSONObject json_data = jsArray.getJSONObject(i);
                appliance = new Appliance();
                appliance.setApplianceName(json_data.getString("Name"));
                appliance.setStartTime(json_data.getString("Start_Time"));
                appliance.setEndTime(json_data.getString("End_Time"));
                appliance.setDuration(json_data.getString("Duration"));
                appliance.setWatts(json_data.getString("Wattage"));

                appliances.add(appliance);


                appList.add(json_data.getString("Name"));

            }
        }
        catch(JSONException e){
            //Log.e("log_tag", "Error parsing data "+e.toString());
            Log.e("log_tag", "Error parsing data "+e.toString());
        }

        String b = "";
        for(int j = 0; j < appliances.size();j++)
        {
            Appliance a = new Appliance();
            a = appliances.get(j);


            b = b + StringUtils.rightPad(a.getApplianceName(), 16) + "\t" + StringUtils.rightPad(a.getStartTime(), 6) +
                    "\t" + StringUtils.rightPad(a.getEndTime(), 6) + "\t" + StringUtils.rightPad(a.getDuration(), 5) + "\t"
                    + a.getWatts() + "\n\n";

        }
        show.setTypeface(Typeface.MONOSPACE);
        show.setText("");
        show.setText(b);
        bundle.putStringArrayList("ListAppliance", appList);
    }

    public String getConnection(String url, String request, String usr){

        Bundle bundle = new Bundle();
        Message msg = new Message();
        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("request",request));
        nameValuePairs1.add(new BasicNameValuePair("username",usr));

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();
            // have to change the ip here to your ip
            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
        //convert response to string
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result "+e.toString());
        }
        return result;

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}
