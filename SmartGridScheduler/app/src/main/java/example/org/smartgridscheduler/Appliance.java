package example.org.smartgridscheduler;

public class Appliance {
    private String applianceName;
    private String startTime, endTime, duration, watts;

    public String getApplianceName() {
        return applianceName;
    }
    public void setApplianceName(String applianceName) {
        this.applianceName = applianceName;
    }
    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    public String getDuration() {
        return duration;
    }
    public void setDuration(String duration) {
        this.duration = duration;
    }
    public String getWatts() {
        return watts;
    }
    public void setWatts(String watts) {
        this.watts = watts;
    }
}
