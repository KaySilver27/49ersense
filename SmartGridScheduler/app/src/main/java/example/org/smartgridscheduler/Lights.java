package example.org.smartgridscheduler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Lights extends AppCompatActivity {

    private static SeekBar seek_bar1;
    private static SeekBar seek_bar2;
    private static SeekBar seek_bar3;
    private static TextView text_view1;
    private static TextView text_view2;
    private static TextView text_view3;
    int progress_value = 0;
    private static String ipAddress = "192.168.0.46";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lights);
        seekbar();
        getSeekbar1Value();
        getSeekbar2Value();
        getSeekbar3Value();
    }

    public void  getSeekbar1Value()
    {
        String updateString = "rd-0-3-1" + '\n';
        // set is to set the value in database
        // 0 - ground floor

        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("update", updateString));

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();

            // have to change the ip here to correct ip
            HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection "+e.toString());
            Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
            String[] arr = result.split("\n");


            seek_bar1.setProgress(Integer.parseInt(arr[0]));

        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result " + e.toString());
        }

    }

    public void  getSeekbar2Value()
    {
        String updateString = "rd-0-3-2" + '\n';
        // set is to set the value in database
        // 0 - ground floor

        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("update", updateString));

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();

            // have to change the ip here to correct ip
            HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection "+e.toString());
            Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
            String[] arr = result.split("\n");


            seek_bar2.setProgress(Integer.parseInt(arr[0]));

        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result " + e.toString());
        }

    }

    public void  getSeekbar3Value()
    {
        String updateString = "rd-0-3-3" + '\n';
        // set is to set the value in database
        // 0 - ground floor

        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("update", updateString));

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();

            // have to change the ip here to correct ip
            HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection "+e.toString());
            Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
            String[] arr = result.split("\n");


            seek_bar3.setProgress(Integer.parseInt(arr[0]));

        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result " + e.toString());
        }

    }


    public void seekbar(){
        seek_bar1 = (SeekBar)findViewById(R.id.seekBar);
        seek_bar2 = (SeekBar)findViewById(R.id.seekBar2);
        seek_bar3 = (SeekBar)findViewById(R.id.seekBar3);
        text_view1 = (TextView)findViewById(R.id.textView29);
        text_view2 = (TextView)findViewById(R.id.textView30);
        text_view3 = (TextView)findViewById(R.id.textView31);
        text_view1.setText("Brightness: " + seek_bar1.getProgress() + "/" + seek_bar1.getMax());
        text_view2.setText("Brightness: " + seek_bar2.getProgress() + "/" + seek_bar2.getMax());
        text_view3.setText("Brightness: " + seek_bar3.getProgress() + "/" + seek_bar3.getMax());


        seek_bar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {



                    @Override
                    public void onProgressChanged(SeekBar seekBar,int progress, boolean fromuser){
                        progress_value = progress;
                        text_view1.setText("Brightness: " + progress_value + "/" + seek_bar1.getMax());
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar){


                    }
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        text_view1.setText("Brightness: " + progress_value + "/" + seek_bar1.getMax());

                        String updateString = "set-0-3-1-"+progress_value+'\n';
                        // set is to set the value in database
                        // 0 - ground floor

                        InputStream inputStream = null;
                        String result = "";
                        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
                        nameValuePairs1.add(new BasicNameValuePair("update", updateString));

                        //http postappSpinners
                        try{
                            HttpClient httpclient = new DefaultHttpClient();

                            // have to change the ip here to correct ip
                            HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
                            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
                            HttpResponse response = httpclient.execute(httppost);
                            HttpEntity entity = response.getEntity();
                            inputStream = entity.getContent();
                        }
                        catch(Exception e){
                            Log.e("log_tag", "Error in http connection "+e.toString());
                            Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

                        }

                        try{
                            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
                            StringBuilder sb = new StringBuilder();
                            String line = null;
                            while ((line = reader.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                            inputStream.close();
                            result=sb.toString();

                            if (result.contains("true"))
                            {
                                Toast.makeText(getBaseContext(), "Update Successful", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(getBaseContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch(Exception e){
                            Log.e("log_tag", "Error converting result " + e.toString());
                        }

                    }
                }
        );


        seek_bar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress_value;
            @Override
            public void onProgressChanged(SeekBar seekBar,int progress, boolean fromuser){
                progress_value = progress;
                text_view2.setText("Brightness: " + progress_value + "/" + seek_bar2.getMax());
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar){

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                text_view2.setText("Brightness: " + progress_value + "/" + seek_bar2.getMax());
                String updateString = "set-0-3-2-"+progress_value+'\n';
                // set is to set the value in database
                // 0 - ground floor

                InputStream inputStream = null;
                String result = "";
                ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
                nameValuePairs1.add(new BasicNameValuePair("update", updateString));

                //http postappSpinners
                try{
                    HttpClient httpclient = new DefaultHttpClient();

                    // have to change the ip here to correct ip
                    HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    inputStream = entity.getContent();
                }
                catch(Exception e){
                    Log.e("log_tag", "Error in http connection "+e.toString());
                    Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

                }

                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result=sb.toString();
                    if (result.contains("true"))
                    {
                        Toast.makeText(getBaseContext(), "Update Successful", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                    }
                }
                catch(Exception e){
                    Log.e("log_tag", "Error converting result " + e.toString());
                }


            }
        }
        );


        seek_bar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress_value;
            @Override
            public void onProgressChanged(SeekBar seekBar,int progress, boolean fromuser){
                progress_value = progress;
                text_view3.setText("Brightness: " + progress_value + "/" + seek_bar3.getMax());
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar){

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                text_view3.setText("Brightness: " + progress_value + "/" + seek_bar3.getMax());

                String updateString = "set-0-3-3-"+progress_value+'\n';
                // set is to set the value in database
                // 0 - ground floor

                InputStream inputStream = null;
                String result = "";
                ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
                nameValuePairs1.add(new BasicNameValuePair("update", updateString));

                //http postappSpinners
                try{
                    HttpClient httpclient = new DefaultHttpClient();

                    // have to change the ip here to correct ip
                    HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    inputStream = entity.getContent();
                }
                catch(Exception e){
                    Log.e("log_tag", "Error in http connection "+e.toString());
                    Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

                }

                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result=sb.toString();
                    if (result.contains("true"))
                    {
                        Toast.makeText(getBaseContext(), "Update Successful", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                    }
                }
                catch(Exception e){
                    Log.e("log_tag", "Error converting result " + e.toString());
                }


            }
        }
        );


    }
}
