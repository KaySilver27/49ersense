package example.org.smartgridscheduler;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class EditAppliance extends Activity{
    private Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_appliance);

        bundle = getIntent().getExtras();
        final String app_name = bundle.getString("app_name_select");
        final String username = bundle.getString("username");
        final Button backButton = (Button) findViewById(R.id.button_back);

        final Button submitButton = (Button) findViewById(R.id.btn_submit);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final EditText appName = (EditText) findViewById(R.id.app_name);
        appName.setText(app_name);

        final EditText startTime = (EditText) findViewById(R.id.startTime);
        final EditText endTime = (EditText) findViewById(R.id.endTime);
        final EditText duration = (EditText) findViewById(R.id.editText9);
        final EditText watt = (EditText) findViewById(R.id.watt);

        String conResult = getConnection (MainActivity.ipAddress+"editAppliance.php","retrieve",username,app_name,"","","","");

        try{
            JSONArray jArray = new JSONArray(conResult);

            //for(int i=0;i<jArray.length();i++){
            for(int i=0;i<jArray.length();i++){
                JSONObject json_data = jArray.getJSONObject(i);

                String start_time =json_data.getString("Start_Time");
                String end_time =json_data.getString("End_Time");
                String run_time =json_data.getString("Duration");
                String watts =json_data.getString("Wattage");

                startTime.setText(start_time);
                endTime.setText(end_time);
                duration.setText(run_time);
                watt.setText(watts);
            }
        }

        catch(JSONException e){
            //Log.e("log_tag", "Error parsing data "+e.toString());
            Log.e("log_tag", "Error parsing data "+e.toString());
        }

        Button resetButton = (Button) findViewById(R.id.button4);

        resetButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                appName.setText("");
                appName.setHint("Enter Appliance Name:");
                startTime.setText("");
                endTime.setText("");
                duration.setText("");
                watt.setText("");
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //gettting current string values from the view
                String appname = appName.getText().toString();
                String start = startTime.getText().toString();
                String end = endTime.getText().toString();
                String time = duration.getText().toString();
                String power = watt.getText().toString();

                getConnection(MainActivity.ipAddress+"editAppliance.php", "update", username, appname,start,end,time,power);
                Toast.makeText(getBaseContext(), "Appliance Saved!", Toast.LENGTH_SHORT).show();
                finish();

            }
        });


    }

    public String getConnection(String url, String request, String usr, String app,
                                String start, String end ,String time, String watts){

        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("request",request));
        nameValuePairs1.add(new BasicNameValuePair("username", usr));
        nameValuePairs1.add(new BasicNameValuePair("appname",app));

        if(request.equals("update")){
            nameValuePairs1.add(new BasicNameValuePair("starttime",start));
            nameValuePairs1.add(new BasicNameValuePair("endtime",end));
            nameValuePairs1.add(new BasicNameValuePair("duration",time));
            nameValuePairs1.add(new BasicNameValuePair("watts",watts));
        }

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();

            // have to change the ip here to your ip
            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
        //convert response to string
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result "+e.toString());
        }
        return result;

    }
}
