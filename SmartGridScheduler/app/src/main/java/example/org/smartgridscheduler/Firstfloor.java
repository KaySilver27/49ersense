package example.org.smartgridscheduler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Firstfloor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstfloor);
        MainActivity.floor = 1;
    }
}
