package example.org.smartgridscheduler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;

public class EditApplianceActivity extends Activity implements AdapterView.OnItemSelectedListener{

    private Handler handler;
    private Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_appliance_activity);

        bundle = getIntent().getExtras();
        final ArrayList<String> appList = bundle.getStringArrayList("ListAppliance");
        final String username = bundle.getString("username");

        final Spinner appliance = (Spinner) findViewById(R.id.app_spinner);

        final Button editButton = (Button) findViewById(R.id.btn_edit);
        final Button backButton = (Button) findViewById(R.id.btn_back);

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditApplianceActivity.this,
                android.R.layout.simple_spinner_item,appList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        appliance.setAdapter(adapter);

        editButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String app_name = appliance.getSelectedItem().toString();

                bundle = getIntent().getExtras();
                bundle.putString("app_name_select", app_name);
                Intent editAppliance = new Intent(getBaseContext(), EditAppliance.class);
                editAppliance.putExtras(bundle);
                startActivity(editAppliance);
                finish();
            }
        });

    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


}
