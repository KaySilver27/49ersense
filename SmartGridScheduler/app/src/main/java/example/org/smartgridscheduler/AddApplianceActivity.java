package example.org.smartgridscheduler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;

public class AddApplianceActivity extends Activity implements AdapterView.OnItemSelectedListener{
    /**
     * Called when the activity is first created.
     */

    // this spin loads the houses as an array
    static ArrayList<String> houseList = new ArrayList<String>();

    // loads the appliances
    static ArrayList<String> appList = new ArrayList<String>();
    static ArrayList<Appliance> appliances = new ArrayList<Appliance>();
    private String[] attributes = {"Appliance ID", "Lumens", "Size", "Voltage", "Watts"};
    static int housePosition, attributePosition, appliancePosition;
    static String selAppliance;
    ;
    static int watts, Lumens, startTime, endTime;
    private Handler handler;
    private Bundle bundle;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_appliance_activity);

        final Spinner appliance = (Spinner) findViewById(R.id.app_spinner);

        final Button addButton = (Button) findViewById(R.id.btn_add);
        final Button backButton = (Button) findViewById(R.id.btn_back);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.appliance_name, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        appliance.setAdapter(adapter);

        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String app_name = appliance.getSelectedItem().toString();

                bundle = getIntent().getExtras();
                bundle.putString("app_name", app_name);
                Intent addAppliance = new Intent(getBaseContext(), AddAppliance.class);
                addAppliance.putExtras(bundle);
                startActivity(addAppliance);
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }



}