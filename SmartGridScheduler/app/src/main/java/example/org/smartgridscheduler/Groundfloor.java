package example.org.smartgridscheduler;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static example.org.smartgridscheduler.MainActivity.floor;

public class Groundfloor extends AppCompatActivity {
    private static Button button_sbm2;
    private static Button button_sbm3;
    private static Button button_sbm4;
    private static Button button_sbm5;
    private static Button button_sbm6;
    private static Button button_sbm7;
    private static Button button_sbm8;
    private static Button button_sbm9;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groundfloor);
        OnClickButtonListener();
        MainActivity.floor = 0;
    }
    public void OnClickButtonListener(){
        button_sbm2 = (Button)findViewById(R.id.buttonsecuritygrnd);
        button_sbm2.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent security_system = new Intent(getBaseContext(),Securitysystem.class);
                        startActivity(security_system);
                    }
                }
        );

        button_sbm3 = (Button)findViewById(R.id.buttonlightgnd);
        button_sbm3.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent lights = new Intent(getBaseContext(),Lights.class);
                        startActivity(lights);
                    }
                }
        );

        button_sbm4 = (Button)findViewById(R.id.buttongaragegnd);
        button_sbm4.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent garage = new Intent(getBaseContext(),GarageActivity.class);
                        startActivity(garage);
                    }
                }
        );

        button_sbm5 = (Button)findViewById(R.id.buttonthermognd);
        button_sbm5.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent thermo = new Intent(getBaseContext(),ThermostatActivity.class);
                        startActivity(thermo);
                    }
                }
        );

        button_sbm6 = (Button)findViewById(R.id.buttonlocksgnd);
        button_sbm6.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent locks = new Intent(getBaseContext(),LocksActivity.class);
                        startActivity(locks);
                    }
                }
        );

        button_sbm7 = (Button)findViewById(R.id.buttondoorsensegnd);
        button_sbm7.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent doors = new Intent(getBaseContext(),Doorswindows.class);
                        startActivity(doors);
                    }
                }
        );

        button_sbm8 = (Button)findViewById(R.id.buttonmotiongnd);
        button_sbm8.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent motionsensor = new Intent(getBaseContext(),Motionsensor.class);
                        startActivity(motionsensor);
                    }
                }
        );



    }
}
