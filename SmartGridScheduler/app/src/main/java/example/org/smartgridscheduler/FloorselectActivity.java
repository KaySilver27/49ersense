package example.org.smartgridscheduler;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FloorselectActivity extends AppCompatActivity {

    private static Button button_sbm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floorselect);
        OnClickButtonListener();

    }

    public void OnClickButtonListener(){
        button_sbm = (Button)findViewById(R.id.button5);
        button_sbm.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View V){
                        Intent groundfloor = new Intent(getBaseContext(),Groundfloor.class);
                        startActivity(groundfloor);
                    }
                }
        );
    }

}
