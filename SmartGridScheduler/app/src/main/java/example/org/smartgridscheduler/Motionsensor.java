package example.org.smartgridscheduler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static example.org.smartgridscheduler.R.styleable.View;

public class Motionsensor extends AppCompatActivity {

    private static String ipAddress = "192.168.1.3";
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motionsensor);
        Spinner mySpinner=(Spinner) findViewById(R.id.spinner2);
        adapter = ArrayAdapter.createFromResource(this,R.array.motionsensorstatus,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(adapter);
        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value;
                Toast.makeText(getBaseContext(),parent.getItemAtPosition(position)+ " selected" , Toast.LENGTH_SHORT).show();//for testing
                if(position == 0)
                    value = "Active";
                else
                    value = "Inactive";
                String updateString = "set-0-6-"+value+'\n'; //change set value
                // set is to set the value in database
                // 0 - ground floor

                InputStream inputStream = null;
                String result = "";
                ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
                nameValuePairs1.add(new BasicNameValuePair("update", updateString));

                //http postappSpinners
                try{
                    HttpClient httpclient = new DefaultHttpClient();

                    // have to change the ip here to correct ip
                    HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    inputStream = entity.getContent();
                }
                catch(Exception e){
                    Log.e("log_tag", "Error in http connection "+e.toString());
                    Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

                }

                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result=sb.toString();

                    if (result.contains("true"))
                    {
                        Toast.makeText(getBaseContext(), "Update Successful", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                    }
                }
                catch(Exception e){
                    Log.e("log_tag", "Error converting result " + e.toString());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }
        );
        spinnersetvalue();
    }
    void spinnersetvalue()
    {
        String updateString = "rd-0-6" + '\n';
        // set is to set the value in database
        // 0 - ground floor

        InputStream inputStream = null;
        String result = "";
        ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
        nameValuePairs1.add(new BasicNameValuePair("update", updateString));

        //http postappSpinners
        try{
            HttpClient httpclient = new DefaultHttpClient();

            // have to change the ip here to correct ip
            HttpPost httppost = new HttpPost("http://"+ipAddress+"/UpdateRetrieveIOT.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection "+e.toString());
            Toast.makeText(getBaseContext(), "Server Not Responding", Toast.LENGTH_SHORT).show();

        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            result=sb.toString();
            String[] arr = result.split("\n");
            Spinner mySpinner=(Spinner) findViewById(R.id.spinner2);
            int index =0;
            if(arr[0].equals("Active"))
                index = 0;
            else if(arr[0].equals("Inactive"))
                index = 1;
            mySpinner.setSelection(index);


            //seek_bar1.setProgress(Integer.parseInt(arr[0]));

        }
        catch(Exception e){
            Log.e("log_tag", "Error converting result " + e.toString());
        }

    }
    /*Testing purpose*/
    void spinnerselectvalue()
    {
        Spinner mySpinner=(Spinner) findViewById(R.id.spinner2);
        mySpinner.setSelection(1);
    }

}
