package example.org.smartgridscheduler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class UserHomeActivity extends Activity{
    private Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);

        setContentView(R.layout.user_home);

        final Button applianceButton = (Button) findViewById(R.id.manage_app);
        final Button scheduleButton = (Button) findViewById(R.id.manage_sch);
        final Button viewSchedule = (Button) findViewById(R.id.view_sch);
        final Button accountButton = (Button) findViewById(R.id.manage_acc);
        final Button logoutButton = (Button) findViewById(R.id.logout_btn);

        final TextView userName = (TextView) findViewById(R.id.username);

        bundle = getIntent().getExtras();
        String user = bundle.getString("username");

        userName.setText("Hello " + user);

        applianceButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                bundle = getIntent().getExtras();
                String username = bundle.getString("username");
                bundle.putString("username1", username);
                Intent manageAppliance = new Intent(getBaseContext(), ManageAppliance.class);
                manageAppliance.putExtras(bundle);
                startActivity(manageAppliance);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        accountButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                bundle = getIntent().getExtras();
                Intent accountActivity = new Intent(getBaseContext(),ManageAccountActivity.class);
                accountActivity.putExtras(bundle);
                startActivity(accountActivity);
            }
        });

    }

}
